FactoryGirl.define do
  factory :contact_info do
    mobile "9876543210"
    address_line1 "456/21 Curzon gate tell"
    address_line2 "near tinkonia bus stand"
    landmark "curzon gate"
    state "West bengal"
    country "india"
    pin "654321"
  end
end
