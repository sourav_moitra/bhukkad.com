FactoryGirl.define do
  factory :admin do
    username { generate(:username) }
    email { generate(:email) }
    password "FOOBAR10"
  end

  sequence :username do |n|
    "ABCD#{n}"
  end

  sequence :email do |n|
    "abcd#{n}@abcd.com"
  end
end
