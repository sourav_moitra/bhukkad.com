FactoryGirl.define do
  factory :city do
    name { generate(:burdwan) }
    subtitle "Rice bowl of Bengal"
    description "Sitabhog and Mihidana are two famous sweets of Burdwan, introduced first in honor of the Raj family. Shaktigarh's Langcha is another local specialty for Shaktigarh In Eastern Part of Burdwan City"
  end

  sequence :burdwan do |n|
    "Burdwan#{n}"
  end
end
