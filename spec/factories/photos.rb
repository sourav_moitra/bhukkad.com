FactoryGirl.define do
  factory :photo do
    imagable_id 1
    imagable_type "MyString"
    image "MyString"
  end
end
