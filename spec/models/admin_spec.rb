require 'rails_helper'

RSpec.describe Admin, type: :model do
  describe "#username" do
    it "should be present" do
      admin = build(:admin, username: "")
      expect(admin).not_to be_valid
    end

    it "should be unique" do
      create(:admin, username: "ABCD")
      admin = build(:admin, username: "ABCD")
      expect(admin).not_to be_valid
    end

    it "should be under 20 characters" do
      admin = build(:admin, username: "A" * 30)
      expect(admin).not_to be_valid
    end
  end
end
