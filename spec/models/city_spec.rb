require 'rails_helper'

RSpec.describe City, type: :model do
  describe "#name" do
    it "should have name" do
      city = build(:city, name: "")
      expect(city).not_to be_valid
    end

    it "should be unique" do
      city = create(:city, name: "B1")
      invalid_city = build(:city, name: "B1")
      expect(invalid_city).not_to be_valid
    end
  end

  describe "#slug" do
    it "should be unique" do
      city = create(:city, name: "B1")
      invalid_city = build(:city, name: "B1")
      expect(invalid_city).not_to be_valid
    end
  end

  describe "#slugify" do
    it "should create a slug" do
      city = create(:city)
      expect(city.slug).to eq(city.name.downcase.gsub(" ", "-"))
    end
  end

  describe "#to_name" do
    it "should return slug" do
      city = create(:city)
      expect(city.to_param).to eq(city.slug)
    end
  end

  describe "#find_city" do
    it "should find cities based on slug" do
      city = create(:city, name: "test")
      expect(City.find_city(city.slug)).to eq(City.find_by(slug: city.slug))
    end
  end
end
