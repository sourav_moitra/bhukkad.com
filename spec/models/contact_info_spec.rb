require 'rails_helper'

RSpec.describe ContactInfo, type: :model do
  describe "#mobile" do
    it "should be present" do
      resturant = build(:contact_info, mobile: "")
      expect(resturant).not_to be_valid
    end
  end

  describe "#address_line1" do
    it "should be present" do
      resturant = build(:contact_info, address_line1: "")
      expect(resturant).not_to be_valid
    end
  end

  describe "#address_line2" do
    it "should be present" do
      resturant = build(:contact_info, address_line2: "")
      expect(resturant).not_to be_valid
    end
  end

  describe "#landmark" do
    it "should be present" do
      resturant = build(:contact_info, landmark: "")
      expect(resturant).not_to be_valid
    end
  end

  describe "#pin" do
    it "should be present" do
      resturant = build(:contact_info, pin: "")
      expect(resturant).not_to be_valid
    end
  end

  describe "#state" do
    it "should be present" do
      resturant = build(:contact_info, state: "")
      expect(resturant).not_to be_valid
    end
  end

  describe "#country" do
    it "should be present" do
      resturant = build(:contact_info, country: "")
      expect(resturant).not_to be_valid
    end
  end
end
