require 'rails_helper'

RSpec.describe Resturant, type: :model do
  describe "#name" do
    it "should have name" do
      resturant = build(:resturant, name: "")
      expect(resturant).not_to be_valid
    end

    it "should not be too long" do
      resturant = build(:resturant, name: "a" * 101)
      expect(resturant).not_to be_valid
    end
  end

  describe "#slug" do
    it "should be unique" do
      resturant = create(:resturant, slug: "B1")
      invalid_resturant = build(:resturant, slug: "B1")
      expect(invalid_resturant).not_to be_valid
    end
  end

  describe "#slugify" do
    it "should create a slug" do
      resturant = create(:resturant)
      expect(resturant.slug).to eq(resturant.name.downcase.gsub(" ", "-"))
    end

    it "should not create slug if its already there" do
      resturant = create(:resturant, slug: "abcd")
      expect(resturant.slug).to eq("abcd")
    end
  end

  describe "#to_name" do
    it "should return slug" do
      resturant = create(:resturant)
      expect(resturant.to_param).to eq(resturant.slug)
    end
  end

  describe "#find_resturant" do
    it "should find resturants based on slug" do
      resturant = create(:resturant, name: "test")
      expect(Resturant.find_resturant(resturant.slug)).to eq(Resturant.find_by(slug: resturant.slug))
    end
  end

  describe "#subtitle" do
    it "should be present" do
      resturant = build(:resturant, subtitle: "")
      expect(resturant).not_to be_valid
    end

    it "should be not longer than 100 characters" do
      resturant = build(:resturant, subtitle: "a" * 101)
      expect(resturant).not_to be_valid
    end
  end

  describe "#description" do
    it "should be present" do
      resturant = build(:resturant, description: "")
      expect(resturant).not_to be_valid
    end

    it "should be not less than 50 characters" do
      resturant = build(:resturant, description: "a" * 49)
      expect(resturant).not_to be_valid
    end

    it "should be not more than 500 characters" do
      resturant = build(:resturant, description: "a" * 501)
      expect(resturant).not_to be_valid
    end
  end

  describe "#start_time" do
    it "should be present" do
      resturant = build(:resturant, start_time: "")
      expect(resturant).not_to be_valid
    end

    it "should be properly formatted" do
      improper_formats = ["1111", "14:50 PM"]
      improper_formats.each do |wrong|
        resturant = build(:resturant, start_time: wrong)
        expect(resturant).not_to be_valid
      end
    end
  end

  describe "#close_time" do
    it "should be present" do
      resturant = build(:resturant, close_time: "")
      expect(resturant).not_to be_valid
    end

    it "should be properly formatted" do
      improper_formats = ["1111", "14:50 PM"]
      improper_formats.each do |wrong|
        resturant = build(:resturant, close_time: wrong)
        expect(resturant).not_to be_valid
      end
    end
  end
end
