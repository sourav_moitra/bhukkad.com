# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
City.create([{ name: 'Burdwan', subtitle: 'Rice bowl of Bengal', description: 'Sitabhoh and Mihidana are famous' }, { name: 'Kolkata', subtitle: 'City of joy', description: 'Roll, Kebaab and rosogolla' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
