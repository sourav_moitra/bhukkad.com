class AddVisibleToMenus < ActiveRecord::Migration
  def change
    add_column :menus, :visible, :boolean, default: false
  end
end
