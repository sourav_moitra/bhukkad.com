class AddVegetarianToCuisines < ActiveRecord::Migration
  def change
    add_column :cuisines, :vegetarian, :boolean
  end
end
