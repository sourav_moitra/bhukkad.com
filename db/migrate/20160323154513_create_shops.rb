class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.references :city, index: true, foreign_key: true
      t.string :name
      t.string :shop_type
      t.string :shop_plan, default: "free"
      t.string :subtitle
      t.string :description
      t.string :start_time
      t.string :close_time
      t.string :slug, index: true
      t.references :admin, index: true, foreign_key: true
      t.boolean :open, default: false
      t.boolean :approved, default: false

      t.timestamps null: false
    end
  end
end
