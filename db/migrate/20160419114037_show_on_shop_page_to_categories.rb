class ShowOnShopPageToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :show_on_shop_page, :boolean, default: false
  end
end
