class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :name, index: true
      t.string :subtitle
      t.text :description
      t.string :slug, index: true

      t.timestamps null: false
    end
  end
end
