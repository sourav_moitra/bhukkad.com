class AddIngredientsToCuisines < ActiveRecord::Migration
  def change
    add_column :cuisines, :ingredient, :text
  end
end
