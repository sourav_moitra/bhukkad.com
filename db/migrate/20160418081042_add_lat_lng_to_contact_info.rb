class AddLatLngToContactInfo < ActiveRecord::Migration
  def change
    add_column :contact_infos, :lat, :decimal, {:precision=>10, :scale=>6}
    add_column :contact_infos, :lng, :decimal, {:precision=>10, :scale=>6}
  end
end
