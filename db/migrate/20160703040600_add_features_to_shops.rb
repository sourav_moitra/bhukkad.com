class AddFeaturesToShops < ActiveRecord::Migration
  def change
    add_column :shops, :music, :boolean
    add_column :shops, :parking, :boolean
    add_column :shops, :security, :boolean
    add_column :shops, :air_conditioner, :boolean
    add_column :shops, :wifi, :boolean
    add_column :shops, :noise, :string
    add_column :shops, :parties, :boolean
    add_column :shops, :dating, :boolean
    add_column :shops, :payment, :string
  end
end
