class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :image
      t.references :imageable, polymorphic: true, index: true
      t.timestamps null: false
    end
  end
end
