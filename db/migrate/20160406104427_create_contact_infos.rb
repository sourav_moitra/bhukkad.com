class CreateContactInfos < ActiveRecord::Migration
  def change
    create_table :contact_infos do |t|
      t.string :mobile
      t.string :address_line1
      t.string :address_line2
      t.string :landmark
      t.string :state
      t.string :country
      t.string :pin
      t.references :shop, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
