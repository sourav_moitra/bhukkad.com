Rails.application.routes.draw do
  post '/rate' => 'rater#create', :as => 'rate'
  devise_for :super_admins,
    controllers: { sessions: 'super_admin/sessions' }
  mount Bootsy::Engine => '/bootsy', as: 'bootsy'
  devise_for :admins,
    controllers: { registrations: 'admin/accounts/registrations', sessions: 'admin/accounts/sessions' }
  namespace :admin do
    root 'shop_dashboard#index'
    resources :shop_dashboard do
      collection do
        get '/settings', to: 'shop_dashboard#edit'
      end
    end
    resources :menu_dashboard
    resources :category_dashboard
    resources :cuisine_dashboard
    post '/categories/', to: 'cuisine_dashboard#categories', as: :categories_json
    resources :shop_photo_dashboard
  end

  devise_for :users,
    controllers: { registrations: 'users/registrations',
      confirmations: 'users/confirmations',
      passwords: 'users/passwords',
      unlocks: 'users/unlocks',
      sessions: 'users/sessions' }
  resources :comments, except: [:index, :show]

  namespace :super_admin do
    root 'city_dashboard#index'
    resources :city_dashboard
    resources :shop_dashboard do
      collection do
        patch 'update_plan'
      end
    end
    resources :messages_dashboard
  end

  resources :cities , except: [:show]
  get '/city/:slug', to: 'cities#show'

  resources :restaurants, only: [:index]

  resources :cafes, except: [:show] do
    resources :cuisines, only: [:show]
  end

  resources :confectioneries, only: [:index]

  resources :cafes, only: [:index]

  resources :search, only: [:index]
  get '/:city_slug/restaurant/:shop_slug', to: 'restaurants#show', as: :restaurant_show
  get '/:city_slug/restaurant/:shop_slug/cuisine/:item_slug', to: 'cuisines#show', as: :restaurant_cuisine_show

  get '/:city_slug/confectionery/:shop_slug', to: 'confectioneries#show', as: :confectionery_show
  get '/:city_slug/confectionery/:shop_slug/item/:item_slug', to: 'cuisines#show', as: :confectionery_item_show

  get '/:city_slug/cafe/:shop_slug', to: 'cafes#show', as: :cafe_show
  get '/:city_slug/cafe/:shop_slug/item/:item_slug', to: 'cuisines#show', as: :cafe_item_show


  get '/:city/restaurants', to: 'restaurants#index', as: :cities_restaurants_index
  get '/:city/confectioneries', to: 'confectioneries#index', as: :cities_confectioneries_index
  get '/:city/cafes', to: 'cafes#index', as: :cities_cafes_index

  root 'basic_pages#index'
  get 'about', to: 'basic_pages#about'
  get 'contact', to: 'messages#new'
  resources :messages, only: [:create]

  get '/:slug', to: 'cities#show', as: :city_show
end
