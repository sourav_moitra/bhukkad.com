class Cuisine < ActiveRecord::Base
  belongs_to :category
  validates :name, presence: true, length: { maximum: 100 }
  validates :description, presence: true, length: { maximum: 10000 }
  validates :description, presence: true, length: { maximum: 10000 }
  validates :price, presence: true, format: { with: /\A\d+(?:\.\d{0,2})?\z/ }
  validate :permitted_cuisines, on: :create
  has_many :photos, as: :imageable
  has_many :comments, as: :commentable
  accepts_nested_attributes_for :photos, allow_destroy: true
  include Bootsy::Container
  include PgSearch
  multisearchable against: [:name, :description]

  ratyrate_rateable "overall"


  def to_param
    slug.parameterize
  end

  before_create :slugify, unless: :slug_exist?

  def slugify
    self.slug = name.parameterize
  end

  def slug_exist?
    !(self.slug.blank?)
  end

  FREE_CUISINE_COUNT = 12

  def permitted_cuisines
    if category.menu.shop.shop_plan.eql?("free") && category.cuisines.count >= FREE_CUISINE_COUNT
        errors[:base] << "Please upgrade to standard plan to have more cuisines"
    end
  end
end
