class Photo < ActiveRecord::Base
  belongs_to :imageable, polymorphic: true
  mount_uploader :image, ImageUploader
  validates :image, presence: true

  after_destroy :remove_img

  validate do |photo|
    PermittedValidator.new(photo).validate
  end


  RESTURANT_FREE_CATEGORY = 3

  class PermittedValidator
    def initialize(photo)
      @photo = photo
    end

    def validate
      if @photo.imageable_type.eql?("Resturant") && Resturant.find(@photo.imageable_id).shop_plan.eql?("free")
        count = Resturant.find(@photo.imageable_id).photos.count
        if count >= RESTURANT_FREE_CATEGORY
          @photo.errors[:base] << "Please upgrade to standard plan to upload more pictures"
        end
      end
    end
  end

  def remove_img
    File.delete("#{Rails.root}/public/#{image_url}")
    File.delete("#{Rails.root}/public/#{image_url(:thumb)}")
    File.delete("#{Rails.root}/public/#{image_url(:large)}")
    File.delete("#{Rails.root}/public/#{image_url(:carousel)}")  
  end
end
