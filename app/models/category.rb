class Category < ActiveRecord::Base
  belongs_to :menu
  belongs_to :shop
  has_many :cuisines, dependent: :destroy
  validates :name, presence: true
  validates :menu_id, presence: true
  validate :test, on: :create
  has_one :photo, as: :imageable

  accepts_nested_attributes_for :photo, allow_destroy: true

  scope :can_be_shown_on_shop, -> { where(show_on_shop_page: true) }

  FREE_CATEGORY_COUNT = 2

  def test
    if menu.shop.shop_plan.eql?("free") && menu.categories.count >= FREE_CATEGORY_COUNT
      errors[:base] << "Please upgrade to standard plan to have more categories"
    end
  end
end
