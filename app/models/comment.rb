class Comment < ActiveRecord::Base
  validates :body, presence: true, length: { maximum: 10000 }
  belongs_to :user
  belongs_to :commentable, polymorphic: true
end
