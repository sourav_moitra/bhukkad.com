class Menu < ActiveRecord::Base
  #associations
  belongs_to :shop
  has_many :categories, dependent: :destroy

  #validations
  validates :name, presence: true
  validates :shop_id, presence: true
  validate :permitted_menus, on: :create

  scope :visible, -> { where(visible: true).order("updated_at DESC") }

  FREE_MENU_COUNT = 1

  private

  def permitted_menus
    if shop && shop.shop_plan.eql?("free") && shop.menus.count >= FREE_MENU_COUNT
      errors[:base] << "Please upgrade to standard plan to have more menus"
    end
  end
end
