class Admin < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :shops, dependent: :destroy
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  validates :username, presence: true, length: { maximum: 20 }, uniqueness: true

  def menus
    shops.joins(:menus).select("menus.name as menu_name, shops.name as shop_name, menus.id as menu_id, menus.*")
  end

  def categories
    shops.joins(menus: [:categories])
    .select("categories.*, shops.name as shop_name, menus.name as menu_name, menus.id as menu_id")
  end

  def cuisines
    shops.joins(menus: [categories: [:cuisines]]).select("cuisines.*, menus.name as menu_name, categories.name as category_name")
  end

  def has_shop?(shop_id)
    shops.collect { |r| r.id }.include?(shop_id)
  end

  def has_menu?(menu_id)
    menus.collect { |menu| menu.menu_id }.include?(menu_id)
  end

  def has_category?(category_id)
    categories.collect { |c| c.id }.include?(category_id)
  end
end
