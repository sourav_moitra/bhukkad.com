class ContactInfo < ActiveRecord::Base
  belongs_to :shop
  validates :mobile, presence: true, length: { is: 10 }, format: { with: /\A[789]\d{9}\z/ }
  validates :address_line1, presence: true
  validates :landmark, presence: true
  validates :pin, presence: true
  validates :country, presence: true
  validates :state, presence: true
end
