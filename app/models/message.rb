class Message < ActiveRecord::Base
  validates :name, presence: true
  validates :email, presence: true, format: { with:  /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }
  validates :body, presence: true, length: { maximum: 10000 }
end
