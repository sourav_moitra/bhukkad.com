class Shop < ActiveRecord::Base
  include PgSearch
  include Bootsy::Container

  #associations
  belongs_to :city
  # Deprecated
  has_many :menus, dependent: :destroy

  has_many :categories
  belongs_to :admin
  has_one :contact_info, dependent: :destroy
  has_many :photos, as: :imageable
  accepts_nested_attributes_for :photos, allow_destroy: true
  accepts_nested_attributes_for :contact_info

  # validations
  validates :name, presence: true, length: { maximum: 100 }
  validates :slug, length: { maximum: 100 }, uniqueness: true
  validates :subtitle, presence: true, length: { maximum: 100 }
  validates :description, presence: true, length: { in: 50..10000 }
  validates :start_time, presence: true, format: { with: /\A([0-9]|[0-9]{1}{1}[0-2]{1}):[0-6]{1}[0-9]{1}\s[A|P][M]/ }
  validates :close_time, presence: true, format: { with: /\A([0-9]|[0-9]{1}{1}[0-2]{1}):[0-6]{1}[0-9]{1}\s[A|P][M]/ }
  validates :shop_type, presence: true, inclusion: { in: SHOP_TYPES }

  #scope
  scope :restaurants, -> { where(shop_type: "Restaurant")}
  scope :cafes, -> { where(shop_type: "Cafe")}
  scope :confectioneries, -> { where(shop_type: "Confectionery")}


  multisearchable against: [:name, :subtitle, :description]

  def to_param
    slug
  end

  def self.find_shop(city_slug, slug)
    city = City.find_by(slug: city_slug)
    self.find_by(city_id: city.id, slug: slug)
  end

  def visible?
    approved && open
  end

  def self.search_restaurants(search, city)
    restaurants = self.where(approved: true, open: true).restaurants
    if search.present?
      restaurants = restaurants.where("name ilike ? or subtitle @@ ? or description @@ ?", "%#{search}%", search, search)
    end
    if city.present?
      city = City.find_by(slug: city)
      restaurants = restaurants.where(city: city)
    end
    restaurants
  end

  def self.search_confectioneries(search, city)
    confectioneries = self.where(approved: true, open: true).confectioneries
    if search.present?
      confectioneries = confectioneries.where("name ilike ? or subtitle @@ ? or description @@ ?", "%#{search}%", search, search)
    end
    if city.present?
      city = City.find_by(slug: city)
      confectioneries = confectioneries.where(city: city)
    end
    confectioneries
  end

  def self.search_cafes(search, city)
    cafes = self.where(approved: true, open: true).cafes
    if search.present?
      cafes = cafes.where("name ilike ? or subtitle @@ ? or description @@ ?", "%#{search}%", search, search)
    end
    if city.present?
      city = City.find_by(slug: city)
      cafes = cafes.where(city: city)
    end
    cafes
  end

  def non_free_shop?
    !shop_plan.eql?("free")
  end

  before_create :slugify, unless: :slug_exist?

  def slugify
    self.slug = name.parameterize
  end

  def slug_exist?
    !(self.slug.blank?)
  end
end
