class City < ActiveRecord::Base

  include PgSearch
  multisearchable against: [:name, :subtitle, :description]

  has_many :shops
  validates :name, presence: true, uniqueness: true
  validates :slug, uniqueness: true

  def to_param
    slug
  end

  def self.find_city(slug)
    self.find_by(slug: slug)
  end

  before_create :slugify

  def slugify
    self.slug = name.parameterize
  end
end
