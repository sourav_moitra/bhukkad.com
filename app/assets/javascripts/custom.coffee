$(document).on 'turbolinks:load', () ->
  $('[data-toggle="tooltip"]').tooltip()
  $("#navbar-search-form").hide()

  $("#navbar-search-icon").on 'click', (e) ->
      e.defaultPrevented
      $("#navbar-search-icon").hide()
      $("#navbar-search-form").show()
