$(document).on "turbolinks:load", ->
  $(".carousel-inner .item:first-child").addClass("active")
  $(".carousel").carousel()
  lat = $("#lat").html().trim()
  lng = $("#lng").html().trim()
  mymap = L.map('mapid').setView([lat, lng], 16)

  defIcon = L.icon({
      iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-icon.png',
      shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png'
  })

  L.tileLayer('https://{s}.{base}.maps.cit.api.here.com/maptile/2.1/{type}/{mapID}/normal.day/{z}/{x}/{y}/{size}/{format}?app_id={app_id}&app_code={app_code}&lg={language}', {
    attribution: 'HERE Maps &copy; 1987-2016 <a href="http://developer.here.com">HERE</a>',
    subdomains: '1234',
    mapID: 'newest',
    base: 'base',
    maxZoom: 20,
    type: 'maptile',
    language: 'eng',
    format: 'png8',
    size: '256'
    app_id: '4tSNI6Mr845qFF1D7uQl',
    app_code: '2oFE7hr3NvtmzK8bwUozjQ'
  }).addTo(mymap)
  marker = L.marker([lat, lng], {icon: defIcon}).addTo(mymap)
