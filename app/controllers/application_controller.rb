class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :dynamic_root_path


  def after_sign_in_path_for(resource)
    if resource.class == User
      relative_root_path = root_path
    elsif resource.class == Admin
      relative_root_path = admin_root_path
    elsif resource.class = SuperAdmin
      relative_root_path = super_admin_root_path
    end

    if (request.referer == new_user_session_url) || (request.referer == new_admin_session_url) || (request.referer == new_super_admin_session_url) 
      super
    else
      stored_location_for(resource) || request.referer || relative_root_path
    end
  end

  def dynamic_root_path
    @city = City.find_by(slug: cookies[:city_slug])
    if request.original_fullpath.eql?("/") && @city
      redirect_to city_show_path(@city)
    end
  end
end
