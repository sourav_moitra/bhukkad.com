class SuperAdmin::ShopDashboardController < ApplicationController

  before_action :authenticate_super_admin!

  def index
    @shops = Shop.all
  end

  def create
    shop = Shop.find(params[:shop_id])
    if params[:approve].eql?("true")
      approved = true
    else
      approved = false
    end
    if shop.update(approved: approved)
      flash[:succes] = "Shop Approved"
    else
      flash[:danger] = "Something went wrong"
    end
    redirect_to :super_admin_shop_dashboard_index
  end

  def update_plan
    sleep 2
    plan = params[:plan]
    shop = Shop.find(params[:id])
    if shop.update(shop_plan: plan)
      render json: { success: true }
    end
  end

  def show
    @shop = Shop.find_by(slug: params[:id])
  end
end
