class SuperAdmin::SessionsController < Devise::SessionsController
  protect_from_forgery

  def after_sign_in_path_for(resource)
    stored_location_for(resource) || request.referer || super_admin_root_path
  end
end
