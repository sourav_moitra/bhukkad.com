class SuperAdmin::CityDashboardController < ApplicationController

  before_action :authenticate_super_admin!

  def index
    @cities = City.all
  end

  def edit
    @city = City.find_city(params[:id])
  end

  def update
    @city = City.find_city(params[:slug])
    if @city.update(city_params)
      flash[:success] = "City updated"
      redirect_to super_admin_city_dashboard_index_path
    else
      flash.now[:danger] = "Some Went wrong"
      render "edit"
    end
  end

  def destroy
    @city = City.find_city(params[:slug])
  end

  def new
    @city = City.new
  end

  def create
    @city = City.create(city_params)
    if @city.save
      flash[:success] = "City created"
      redirect_to super_admin_city_dashboard_path(@city)
    else
      flash.now[:danger] = "Some Went wrong"
      render "new"
    end
  end

  def show
    @city = City.find_city(params[:id])
  end

  private

  def city_params
    params.require(:city).permit(:name, :subtitle, :description)
  end
end
