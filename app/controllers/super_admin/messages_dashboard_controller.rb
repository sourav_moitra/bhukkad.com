class SuperAdmin::MessagesDashboardController < ApplicationController

  before_action :authenticate_super_admin!

  def index
    @messages = Message.order('created_at DESC').paginate(page: params[:page])
  end

end
