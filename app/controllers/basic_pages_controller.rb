class BasicPagesController < ApplicationController
  def index
    @cities = City.joins(:shops).group('cities.id').order('name')
  end

  def about
  end

  def contact
  end
end
