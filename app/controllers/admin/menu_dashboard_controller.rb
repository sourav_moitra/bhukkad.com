class Admin::MenuDashboardController < ApplicationController
  before_action :authenticate_admin!

  def index
    @menus = current_admin.shops.joins(:menus).select("menus.*, shops.name as shop_name")
  end

  def new
    @menu = Menu.new
    @shops = current_admin.shops
  end

  def create
    @menu = Menu.new(menu_params)
    if current_admin.has_shop?(params[:menu][:shop_id].to_i) && @menu.save
      redirect_to :back, alert: "Menu added"
    else
      redirect_to :back, alert: "#{@menu.errors.full_messages.join(', ')}"
    end
  end

  def edit
    @menu = current_admin.shops.joins(:menus).select("menus.*")
    .where("menus.id = ?", params[:id]).first.becomes(Menu)
    @shops = current_admin.shops
  end

  def update
    @menu = current_admin.shops.joins(:menus).select("menus.*")
    .where("menus.id = ?", params[:id]).first.becomes(Menu)
    @menu.shop_id = params[:menu][:shop_id]
    @menu.name = params[:menu][:name]
    @menu.visible = params[:menu][:visible]
    if @menu.shop.admin == current_admin && @menu.save
      redirect_to admin_menu_dashboard_index_path, alert: "Menu updated"
    else
      redirect_to :back, alert: "#{@menu.errors.full_messages.join(', ')}"
    end
  end

  def destroy
    @menu = Menu.find(params[:id])
    if current_admin.has_menu?(@menu.id) && @menu.destroy
      redirect_to :back, alert: "Menu destroy"
    else
      redirect_to :back, alert: "#{@menu.errors.full_messages.join(', ')}"
    end
  end

  private

  def menu_params
    params.require(:menu).permit(:name, :shop_id, :visible)
  end
end
