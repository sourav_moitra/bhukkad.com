class Admin::Accounts::RegistrationsController < Devise::RegistrationsController
  prepend_before_action :check_captcha, only: [:create] if Rails.env.eql?('production')
  private
    def check_captcha
      if verify_recaptcha
        true
      else
        self.resource = resource_class.new sign_up_params
        respond_with_navigational(resource) { render :new }
      end
    end

  def sign_up_params
    params.require(:admin).permit(:username, :email, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:admin).permit(:username, :email, :password, :password_confirmation, :current_password)
  end

  def check_captcha
    if verify_recaptcha
      true
    else
      self.resource = resource_class.new sign_up_params
      respond_with_navigational(resource) { render :new }
    end
  end
end
