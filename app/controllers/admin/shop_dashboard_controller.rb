class Admin::ShopDashboardController < ApplicationController
  before_action :authenticate_admin!

  def index
    @shops = current_admin.shops.joins('left outer join menus on shops.id = menus.shop_id')
    .joins('left outer join categories on menus.id = categories.menu_id')
    .joins('left outer join cuisines on categories.id = cuisines.category_id')
    .group("shops.id")
    .select("shops.*, COUNT(cuisines.id) as cuisines_count").order("shops.name")
  end

  def new
    @shop = Shop.new
    @shop.build_contact_info
  end

  def create
    @shop = Shop.new(shop_params)
    @shop.admin = current_admin
    if @shop.save
      redirect_to admin_root_path, alert: "Restaurant added"
    else
      render "new"
    end
  end

  def edit
    @shops = current_admin.shops
  end

  def update
    city = City.find(params[:shop][:city_id])
    @shop = Shop.find_shop(city.slug, params[:id])
    if @shop.admin === current_admin && @shop.update(shop_params)
      redirect_to :back, alert: "Updated"
    else
      redirect_to :back, notice: "Something went wrong"
    end
  end

  private

  def shop_params
    params.require(:shop).permit(:name, :subtitle, :description, :slug,
    :music, :dating, :security, :payment, :parking, :wifi, :noise, :parties,
    :air_conditioner, :open, :city_id, :start_time, :close_time, :shop_type,
    contact_info_attributes: [:mobile,:address_line1, :address_line2, :landmark,
    :country, :state, :pin, :lat, :lng], photos_attributes: [:id, :_destroy])
  end
end
