class Admin::ShopPhotoDashboardController < ApplicationController
  before_action :authenticate_admin!

  def index
    @shop_photos = current_admin.shops
    .joins("inner join photos on shops.id = photos.imageable_id")
    .where("photos.imageable_type = 'Shop'")
    .group("shops.id")
  end

  def new
    @photo = Photo.new
    @shops = current_admin.shops
  end

  def create
    @photo = Photo.new(photo_params)
    if Shop.find(@photo.imageable_id).admin == current_admin && @photo.save
      redirect_to :back, alert: "Restaurant photo uploaded"
    else
      redirect_to :back, notice: "Errors #{@photo.errors.full_messages.join(', ')}"
    end
  end

  def edit
  end

  def update
  end

  def destroy
    @photo = Photo.find(params[:id])
    if Restaurant.find(@photo.imageable_id).admin == current_admin && @photo.destroy
      redirect_to :back, alert: "Restaurant photo destroyed"
    else
      redirect_to :back, notice: "Errors #{@photo.errors.full_messages.join(', ')}"
    end
  end

  private

  def photo_params
    params.require(:photo).permit(:imageable_id, :image, :imageable_type)
  end
end
