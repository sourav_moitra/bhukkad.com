class Admin::CategoryDashboardController < ApplicationController
  before_action :authenticate_admin!

  def index
    menus = current_admin.menus.collect { |obj| obj.id }
    @categories = Category.where(menu_id: menus)
  end

  def new
    @category = Category.new
    @category.build_photo
    @shops = current_admin.shops
  end

  def create
    @category = Category.new(category_params)
    if current_admin.has_menu?(@category.menu_id) && @category.save
      redirect_to :back, alert: "Category Addded"
    else
      redirect_to :back, alert: "#{@category.errors.full_messages.join(', ')}"
    end
  end

  def edit
    @category = Category.find(params[:id])
    @category.build_photo if @category.photo.nil?
    @shops = current_admin.shops
  end

  def update
    @category = Category.find(params[:id])
    if current_admin.has_menu?(@category.menu_id) && @category.update(category_params)
      redirect_to :back, alert: "Category Updated"
    else
      redirect_to :back, alert: "#{@category.errors.full_messages.join(', ')}"
    end
  end

  def destroy
    @category = Category.find(params[:id])
    if current_admin.has_category?(@category.id) && @category.destroy!
      redirect_to :back, alert: "Category Destroy"
    else
      redirect_to :back, alert: "Something went wrong"
    end
  end

  private

  def category_params
    params.require(:category).permit(:name, :menu_id, :description, :show_on_shop_page,
    photo_attributes: [:id, :_destroy, :image, :imageable_id, :imageable_type])
  end
end
