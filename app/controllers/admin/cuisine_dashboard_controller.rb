class Admin::CuisineDashboardController < ApplicationController
  before_action :authenticate_admin!

  def index
    @cuisines = current_admin.cuisines
  end

  def new
    @cuisine = Cuisine.new
    @cuisine.photos.build
    @menus = current_admin.menus
  end

  def categories
    categories = current_admin.categories.where("menu_id = ?", params[:menu_id])
    render json: categories
  end

  def create
    @cuisine = Cuisine.new(cuisine_params)
    if current_admin.has_category?(@cuisine.category_id) && @cuisine.save
      redirect_to :back, alert: "Cuisine Addded"
    else
      redirect_to :back, notice: "#{@cuisine.errors.full_messages.join(', ')}"
    end
  end

  def edit
    @cuisine = current_admin.cuisines.where("cuisines.slug = ?", params[:id])
    .first.becomes(Cuisine)
    @categories = current_admin.categories
    @cuisine.photos.build if @cuisine.photos.blank?
  end

  # ISSUE this update is overriding validations
  def update
    @cuisine = current_admin.cuisines.where("cuisines.slug = ?", params[:id]).first.becomes(Cuisine)
    if current_admin.has_category?(@cuisine.category_id) && @cuisine.update(cuisine_params)
      redirect_to :back, alert: "Cuisine Updated"
    else
      redirect_to :back, notice: "#{@cuisine.errors.full_messages.join(', ')}"
    end
  end

  def destroy
    cuisine = Cuisine.find(params[:id])
    if cuisine.category.menu.shop.admin == current_admin && cuisine.destroy
      redirect_to :back, alert: "Cuisine Destroyed"
    else
      redirect_to :back, notice: "#{cuisine.errors.full_messages.join(', ')}"
    end
  end

  private

  def cuisine_params
    params.require(:cuisine).permit(:name, :category_id, :price, :description,
    :ingredient, :vegetarian, photos_attributes: [:id, :image, :_destroy])
  end
end
