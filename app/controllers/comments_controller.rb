class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    comment = Comment.new(comments_params)
    comment.user = current_user
    if comment.save
      flash[:success] = "Your comment posted"
      redirect_to :back
    else
      redirect_to :back, alert: "Error: #{comment.errors.full_message.join(', ')}"
    end
  end

  def destroy
  end

  private

  def  comments_params
    params.require(:comment).permit(:body, :commentable_id, :commentable_type)
  end
end
