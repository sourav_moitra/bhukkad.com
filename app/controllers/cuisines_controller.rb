class CuisinesController < ApplicationController

  def show
    @cuisine = Shop.find_shop(params[:city_slug], params[:shop_slug]).menus
    .joins(categories: [:cuisines]).where("cuisines.slug = ?", params[:item_slug])
    .select("cuisines.*").first.becomes(Cuisine)
    @comments = @cuisine.comments.order("updated_at DESC").paginate(:page => params[:page], :per_page => 10)
  end
end
