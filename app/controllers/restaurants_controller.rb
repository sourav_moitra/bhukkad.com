class RestaurantsController < ApplicationController

  def index
    @shops = Shop.search_restaurants(params[:search], params[:city])
  end

  def show
    @shop = Shop.find_shop(params[:city_slug], params[:shop_slug])
    raise ActionController::RoutingError.new("Page not found")  if @shop.nil?
    @photos = @shop.photos
    menus = @shop.menus.collect { |m| m.id }
    @categories = Category.can_be_shown_on_shop.where(menu_id: menus)
    if @shop.menus.visible.any?
      @cuisines = Cuisine.joins(:category).where("categories.menu_id = ?", @shop.menus.visible.first.id)
    end
    render layout: 'layouts/shop_landing'
  end
end
