class CitiesController < ApplicationController
  skip_before_action :dynamic_root_path, only: :show

  def index
    @cities = City.joins(:shops).order('name').distinct
  end

  def show
    @city = City.find_city(params[:slug])
    cookies[:city_slug] = @city.slug
    @restaurants = Shop.restaurants.where(city: @city)
    @confectioneries = Shop.confectioneries.where(city: @city)
    @restaurants = Shop.cafes.where(city: @city)
  end
end
