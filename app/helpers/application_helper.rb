module ApplicationHelper
  def font_awesome(name, title=nil)
    content_tag(:i, '', class: "fa #{name}", "aria-hidden" => 'true', title: title)
  end

  def title(name)
    title = base = "Bhukaad Find a Restaurant, Cafe or Confectionery"
    title = name + " | " + base unless name.empty?
    title
  end

  def result_classified(result)
    r_type = result.searchable_type
    r_id = result.searchable_id
    if r_type.eql?("Shop")
      object = restaurant = Shop.find(r_id)
      path = restaurant_show_path(city_slug: object.city, shop_slug: restaurant.slug)
    elsif r_type.eql?("City")
      object = city = City.find(r_id)
      path = city_show_path(city.slug)
    elsif r_type.eql?("Cuisine")
      object = cuisine = Cuisine.find(r_id)
      path = restaurant_cuisine_show_path(cuisine.category.menu.shop.city, cuisine.category.menu.shop, cuisine)
    end
    { path: path, object: object }
  end

  def open?(open_time, close_time)
    (Time.parse(open_time)..Time.parse(close_time)).cover?(Time.now.getlocal('+05:30'))
  end

  def shop_link(cuisine)
    shop = cuisine.category.menu.shop
    shop_type = shop.shop_type
    p shop_type
    if shop_type.eql?("Restaurant")
      link_to shop.name, restaurant_show_path(shop.city, shop)
    elsif shop_type.eql?("Confectionery")
      link_to shop.name, confectionery_show_path(shop.city, shop)
    else
      link_to shop.name, cafe_show_path(shop.city, shop)
    end
  end

  def user_rating_cuisine(comment, cuisine)
    rate = comment.user.ratings_given.find_by(rateable_type: "Cuisine", rateable_id: cuisine.id)
    if rate.present?
      rate.stars
    else
      "NA"
    end
  end

  def vegetarian(cuisine)
    css_class = cuisine.vegetarian ? "vegetarian" : "nonveg"
     content_tag :div, class: css_class do
       icon("circle")
     end
  end
end
